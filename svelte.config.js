import adapter from '@sveltejs/adapter-auto';
import sveltePreprocess from 'svelte-preprocess';
import { mdsvex } from 'mdsvex';

/** @type {import('@sveltejs/kit').Config} */
const config = {
	extensions: [ '.svelte', '.md' ],

	preprocess: [
		sveltePreprocess({
			scss: {
				includePaths: ['src/lib/styles'],
				// prependData: `@import './src/lib/styles/colors.scss';`
			},
		}),
		mdsvex({
			extensions: ['.md']
		})
	],

	kit: {
		adapter: adapter(),
		// prerender: {
		// 	default: true
		// },

		vite: {
			server: {
				fs: {
					allow: [ 'static/pub' ]
				}
			}
		}
	}
};

export default config;
