import { browser } from "$app/env";
import { writable } from 'svelte/store';

const getLocalStorage = (key, defaultValue) => {
	if (!browser) {
		return defaultValue;
	}
	let storeValue = window.localStorage.getItem(key);
	if (!storeValue) {
		return defaultValue;
	}
	try {
		return JSON.parse(storeValue);
	} catch(e) {
		return defaultValue;
	}
}

const subscribeHandler = (key) => {
	return (value) => {
		if (browser) {
			window.localStorage.setItem(key, JSON.stringify(value));
		}
	}
}

// Array of strings.
export const ccentries = writable(getLocalStorage('ccentries', []))
ccentries.subscribe(subscribeHandler('ccentries'));
